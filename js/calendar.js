//alert("it enters");
"use strict";
var calendar = document.getElementById("calendar");
var mainDate = new Date();
var year = mainDate.getFullYear();
var currentYear = mainDate.getFullYear();
var month = mainDate.getMonth();
var febMnth = 1;
var months = [{ name: "January", days: 31 }, { name: "February", days: febDays(year) }, { name: "March", days: 31 }, { name: "April", days: 30 }, { name: "May", days: 31 }, { name: "June", days: 30 }, { name: "July", days: 31 }, { name: "August", days: 31 }, { name: "September", days: 30 }, { name: "October", days: 31 }, { name: "November", days: 30 }, { name: "December", days: 31 }];
var weeks = ["Sunday", "Monday", "Tueaday", "Wednesday", "Thursday", "Friday", "Saturday"];
var monthName = "";
var monthDays = 0;
var innerHTML = "";
var monthNumbers = "";
var btnsty = "btnsty";

var dayActive = "";
var currentDate = mainDate.getDate();
var currentMonth = mainDate.getMonth();
var currentYear = mainDate.getFullYear();

//console.log(months);
monthsPrint(year, month);

function febDays(year) {
    if ((year % 100 !== 0) && (year % 4 === 0) || (year % 400 === 0)) {
        return 29;
    } else {
        return 28;
    }
}

function monthsPrint(year, month) {
    var months = [{ name: "January", days: 31 }, { name: "February", days: febDays(year) }, { name: "March", days: 31 }, { name: "April", days: 30 }, { name: "May", days: 31 }, { name: "June", days: 30 }, { name: "July", days: 31 }, { name: "August", days: 31 }, { name: "September", days: 30 }, { name: "October", days: 31 }, { name: "November", days: 30 }, { name: "December", days: 31 }];
    for (var i = 0; i < 12; i++) {
        monthName = months[i].name;
        monthDays = months[i].days;

        //alert("month name" + month + "month days" + monthDays);
        if (month == i) {
            var shoWClass = "showClass";
        } else {
            var shoWClass = "hideClass";
        } /*"+monthDays+" "+monthNumbers+"*/
        innerHTML += "<div class='" + shoWClass +
            "' id='myTable'><div class='weeks'>";
        //weekPrint();
        innerHTML += "</div>";
        daysPrint(i);
        innerHTML += "</div>";

    }
    calendar.innerHTML = innerHTML;
    printDateFormat(months);

}

function weekPrint() {
    console.log(weeks.length);
    for (var k = 0; k < weeks.length; k++) {
        innerHTML += "<span>" + weeks[k].substring(0, 3) + "</span>";
    }
}

function daysPrint(i) {
    /** passing object **/
    var colorObj = [{ color: 'green', id: 1, }, { color: 'red', id: 2, }, { color: 'yellow', id: 3 }, {
            color: 'gre',
            id: 4
        }, { color: 'red', id: 5 }, { color: 'yellow', id: 6 }, { color: 'green', id: 7 }, { color: 'gre', id: 8 },
        { color: 'red', id: 9 }, { color: 'gre', id: 10 }, { color: 'yellow', id: 11 },
        { color: 'green', id: 12, }, { color: 'red', id: 13, }, { color: 'yellow', id: 14 }, {
            color: 'gre',
            id: 15
        }, { color: 'red', id: 16 }, { color: 'yellow', id: 17 }, { color: 'green', id: 18 }, { color: 'gre', id: 19 },
        { color: 'red', id: 20 }, { color: 'gre', id: 21 }, { color: 'green', id: 22, }, { color: 'red', id: 23, }, { color: 'yellow', id: 24 }, {
            color: 'gre',
            id: 25
        }, { color: 'red', id: 26 }, { color: 'yellow', id: 27 }, { color: 'green', id: 28 }, { color: 'gre', id: 29 },
        { color: 'red', id: 30 }, { color: 'gre', id: 31 },

    ];

    for (var j = 1; j <= monthDays; j++) {

        var currentDay = new Date((i + 1) + ", " + j + ", " + year);
        var monthStart = new Date((i + 1) + "," + 1 + ", " + year);
        var n = currentDay.getDay();
        var s = monthStart.getDay();
        var totalDays = 7 - s;
        var dayActive = "";
        var disabled = "";
        var btnCls = "";

        for (let ii = 0; ii < colorObj.length; ii++) {
            const colorrr = colorObj[ii];
            if (colorrr.id === j && j < currentDate) {
                btnCls = colorrr.color;
                break;
            } else {
                btnCls = "eachrow";
            }
        }
        if (currentYear == year) {
            if (i == currentMonth && j > currentDate) {
                dayActive = "prevDay";
                disabled = "disabled";
            } else if (i > currentMonth) {
                dayActive = "forwardDay";
                disabled = "";
            } else if (i < currentMonth) {
                dayActive = "prevDay";
                disabled = "disabled";
            } else if (j == currentDate) {
                dayActive = "dayActive";
                disabled = "";
            }
        } else if (year < currentYear) {
            dayActive = "prevDay";
        }

        innerHTML +=
            "<span data-day='" + j + "/" +
            (month + 1) + "/" + year + "' class='  " +
            dayActive + " '><button class='" + btnCls + " " + btnsty + "' id='eachDay' " +
            disabled + "'>" + j + "</button></span>";
    }
}

function addCurrendDateClass(i, j) {
    var dayActive = "";
    var currentDate = mainDate.getDate();
    var currentMonth = mainDate.getMonth();
    var currentYear = mainDate.getFullYear();
    if (currentMonth == i) {
        if (i <= currentMonth && j < currentDate) {
            dayActive = "prevDay";
            // alert(dayActive);
        } else if (currentDate == j) {
            dayActive = "dayActive";
            // alert(dayActive);

        } else {
            dayActive = "forwardDay";
            // alert(dayActive);

        }
    }
}


function nextYear() {
    //year++;
    month++;
    if (month > 11) {
        month = 0;
        year++;
    }
    innerHTML = "";
    monthsPrint(year, month);
    if ($("#dateValue").val() != '') {
        $("span[data-day='" + $("#dateValue").val() + "']").addClass("selected");
    }
}

function prevYear() {
    month--;
    if (month < 0) {
        month = 11;
        year--;
    }
    innerHTML = "";
    monthsPrint(year, month);
    if ($("#dateValue").val() != '') {
        $("span[data-day='" + $("#dateValue").val() + "']").addClass("selected");
    }
}

function printDateFormat(months) {
    $("button").each(function() {
        $(this).click(function() {
            $("button").parent().removeClass("selected");
            $(this).parent().addClass("selected");
            $("#dateValue").val($(this).html() + "/" + (month + 1) + "/" + year);
        });
    });
}